<?php

/**
 * Performs ajax actions for chat widget
 */
class AjaxController extends Controller
{
    /**
     * Returns the filter configurations.
     */
    public function filters()
    {
        return array(
            'ajaxOnly + index, create',
            'postOnly + create',
        );
    }

    /**
     * This method is invoked right before an action is to be executed (after all possible filters.)
     *
     * @return boolean whether the action should be executed.
     */
    protected function beforeAction($action)
    {
        header('Content-type: application/json');

        return parent::beforeAction($action);
    }

    /**
     * Renders json result of latest chat messages
     *
     * @param int $id only messages started from specified id would be returned
     */
    public function actionIndex($id = null)
    {
        $result = array();

        /**
         * @var ChatMessage[] $messages
         */
        $messages = ChatMessage::model()->fromId($id)->latest()->findAll();

        foreach ($messages as $message) {
            array_unshift(
                $result,
                array(
                    'id' => (int)$message->id,
                    'datetime' => Yii::app()->format->formatDatetime($message->create_time),
                    'user' => $message->author_id ? $message->author->username : 'Guest',
                    'message' => $message->message,
                )
            );
        }

        echo CJSON::encode($result);
        Yii::app()->end();
    }

    /**
     * Creates new chat message and returns latest chat messages
     *
     * @param int $id only messages started from specified id would be returned
     */
    public function actionCreate($id = null)
    {
        if (($message = Yii::app()->request->getPost('message'))) {
            $model = new ChatMessage();
            $model->message = $message;
            $model->save();
        }

        $this->actionIndex($id);
    }
}
