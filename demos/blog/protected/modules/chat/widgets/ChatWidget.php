<?php

class ChatWidget extends CWidget
{
    private $_messages;

    /**
     * Executes the widget.
     */
    public function run()
    {
        $assetsPath = Yii::getPathOfAlias('chat.assets');
        Yii::app()->clientScript->registerCssFile(
            Yii::app()->getAssetManager()->publish($assetsPath . DIRECTORY_SEPARATOR . 'style.css')
        );
        Yii::app()->clientScript->registerScriptFile(
            Yii::app()->getAssetManager()->publish($assetsPath . DIRECTORY_SEPARATOR . 'chat.js')
        );

        $lastMessage = null;

        $messages = $this->getMessages();
        if (!empty($messages)) {
            $lastMessage = $messages[count($messages) - 1];
        }

        Yii::app()->clientScript->registerScript(
            'chatParams',
            'lastChatMessageId=' . ($lastMessage ? $lastMessage->id : 0) . '; chatMessagesUpdateUrl="' .
                Yii::app()->createUrl('/chat/ajax/index') . '"; maxMessagesCount=' .
                Yii::app()->getModule('chat')->latestMessagesCount . ';'
        );

        $this->render('chat');
    }

    /**
     * @return ChatMessage[]
     */
    public function getMessages()
    {
        if (is_null($this->_messages)) {
            $this->_messages = ChatMessage::model()->latest()->findAll();
            $this->_messages = array_reverse($this->_messages);
        }

        return $this->_messages;
    }
}
