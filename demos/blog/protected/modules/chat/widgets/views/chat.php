<?php
/**
 * @var $this ChatWidget
 */
?>
<div class="chat">
    <div class="workarea" style="display: none">
        <div class="messages">
            <?php foreach ($this->getMessages() as $message): ?>
            <div>
                <strong><?php echo Yii::app()->format->formatDatetime($message->create_time) . ' ' .
                    ($message->author_id ? $message->author->username : 'Guest'); ?></strong>

                <p><?php echo $message->message; ?></p>
            </div>
            <?php endforeach; ?>
        </div>
        <div>
            <form onsubmit="chatSendMessage('<?php echo Yii::app()->createUrl(
                '/chat/ajax/create'
            ); ?>'); return false;">
                <input type="text" id="chatMessage" maxlength="100">
                <button id="chatMessageSubmit" type="submit">SEND</button>
            </form>
        </div>
    </div>
    <div class="arrow">
        <div><a href="#">&lt;</a></div>
    </div>
</div>