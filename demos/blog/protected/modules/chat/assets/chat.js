var lastChatMessageId = 0;
var chatMessagesUpdateUrl;
var maxMessagesCount;
var updateMessagesInterval;

$(document).ready(function () {
    $(".arrow a").click(function () {
        chatWidgetToggle();
        return false;
    });
});

function chatWidgetToggle() {
    if ($(".workarea").is(':hidden')) {
        $('.chat').css('width', 282);
    }

    $(".workarea").toggle("slide", function () {
        if ($(this).is(':hidden')) {
            clearInterval(updateMessagesInterval);
            $(".arrow a").text('<');
            $('.chat').css('width', 30);
        }
        else {
            $('.messages').animate({scrollTop:$('.messages')[0].scrollHeight});
            chatUpdateMessages();
            updateMessagesInterval = setInterval(chatUpdateMessages, 5000);
            $(".arrow a").text('>');
        }
    });
}

function chatSendMessage(url) {
    message = $('#chatMessage').val();
    $('#chatMessage').val('');

    $.post(url + '?id=' + lastChatMessageId, { message:message }, function (data) {
        chatAddMessages(data);
    });
}

function chatUpdateMessages() {
    $.get(chatMessagesUpdateUrl, { id:lastChatMessageId, r:new Date().getTime()}, function (data) {
        chatAddMessages(data);
    });
}

function chatAddMessages(messages) {
    for (key in messages) {
        message = messages[key];

        if (message.id > lastChatMessageId) {
            $("<div><strong>" + message.datetime + " " + message.user + "</strong><p>" + message.message + "</p></div>").appendTo('.messages');
            lastChatMessageId = message.id;
        }
    }

    $('.messages').animate({scrollTop:$('.messages')[0].scrollHeight});

    $('.messages div').slice(0, $('.messages div').length - maxMessagesCount).remove();
}