<?php

class ChatModule extends CWebModule
{
    /**
     * @var string the ID of the default controller for this module. Defaults to 'default'.
     */
    public $defaultController = 'ajax';

    /**
     * @var int count of latest messages would be shown in chat widget
     */
    public $latestMessagesCount = 15;

    /**
     * Initializes the module.
     */
    public function init()
    {
        $this->setImport(
            array(
                'chat.models.*',
            )
        );
    }
}
