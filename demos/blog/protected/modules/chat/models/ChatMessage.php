<?php

/**
 * @property integer $id
 * @property integer $author_id
 * @property integer $create_time
 * @property string  $message
 *
 * @property User    $author
 */
class ChatMessage extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return ChatMessage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{chat_message}}';
    }

    /**
     * Returns a list of behaviors that this model should behave as.
     *
     * @return array the behavior configurations (behavior name=>behavior configuration)
     */
    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'updateAttribute' => null,
            ),
            'AuthorBehavior' => array(
                'class' => 'application.components.AuthorBehavior',
                'editorAttribute' => null,
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('message', 'required'),
            array('message', 'length', 'max' => 100),
        );
    }

    /**
     * Latest messages would be loaded
     *
     * @param int $count count of latest messages. ChatModule::latestMessagesCount by default
     *
     * @return self
     */
    public function latest($count = null)
    {
        if (empty($count)) {
            $count = Yii::app()->getModule('chat')->latestMessagesCount;
        }

        $alias = $this->getTableAlias();
        $criteria = $this->getDbCriteria();

        $criteria->order = $alias . '.id desc';
        $criteria->limit = $count;

        return $this;
    }

    /**
     * Only messages with id, grater than specified id would be loaded
     *
     * @param int $id
     *
     * @return self
     */
    public function fromId($id)
    {
        $id = (int)$id;

        if (!empty($id)) {
            $alias = $this->getTableAlias();
            $criteria = $this->getDbCriteria();

            $criteria->addCondition($alias . '.id > :id');
            $criteria->params['id'] = $id;
        }

        return $this;
    }
}