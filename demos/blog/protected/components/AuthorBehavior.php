<?php

class AuthorBehavior extends CActiveRecordBehavior
{
    public $authorAttribute = 'author_id';
    public $editorAttribute = 'editor_id';
    public $setEditorOnCreate = false;
    public $authorRelationName = 'author';
    public $editorRelationName = 'editor';

    /**
     * @var string name of the author and editor related active record class
     */
    public $relationClassName = 'User';

    /**
     * Attaches the behavior object to the component.
     * @param CActiveRecord $owner the component that this behavior is to be attached to.
     */
    public function attach($owner)
    {
        $metaData = $owner->getMetaData();

        if ($this->authorAttribute && $this->authorRelationName) {
            $metaData->addRelation(
                $this->authorRelationName,
                array(CActiveRecord::BELONGS_TO, $this->relationClassName, $this->authorAttribute)
            );
        }

        if ($this->editorAttribute && $this->editorRelationName) {
            $metaData->addRelation(
                $this->editorRelationName,
                array(CActiveRecord::BELONGS_TO, $this->relationClassName, $this->editorAttribute)
            );
        }

        parent::attach($owner);
    }

    /**
     * Detaches the behavior object from the component.
     * @param CActiveRecord $owner the component that this behavior is to be detached from.
     */
    public function detach($owner)
    {
        $metaData = $owner->getMetaData();

        if ($this->authorAttribute && $this->authorRelationName) {
            $metaData->removeRelation($this->authorRelationName);
        }

        if ($this->editorAttribute && $this->editorRelationName) {
            $metaData->removeRelation($this->editorRelationName);
        }

        parent::detach($owner);
    }

    /**
     * Responds to {@link CActiveRecord::onBeforeSave} event.
     * @param CModelEvent $event event parameter
     */
    public function beforeSave($event)
    {
        $userId = null;

        /**
         * @var CWebUser $userComponent
         */
        $userComponent = Yii::app()->getComponent('user');
        if ($userComponent && !$userComponent->getIsGuest()) {
            $userId = $userComponent->getId();
        }

        /**
         * @var CActiveRecord $owner
         */
        $owner = $this->getOwner();
        if ($owner->getIsNewRecord() && ($this->authorAttribute !== null)) {
            $owner->setAttribute($this->authorAttribute, $userId);
        }
        if ((!$owner->getIsNewRecord() || $this->setEditorOnCreate) && ($this->editorAttribute !== null)) {
            $owner->setAttribute($this->editorAttribute, $userId);
        }
    }
}
