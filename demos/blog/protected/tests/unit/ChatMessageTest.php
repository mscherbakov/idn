<?php

class ChatMessageTest extends CTestCase
{
    function __construct()
    {
        Yii::import('application.modules.chat.models.ChatMessage');
    }

    public function testValidation()
    {
        $chatMessage = new ChatMessage();

        //message required
        $this->assertFalse($chatMessage->validate());
        $chatMessage->message = 'a';
        $this->assertTrue($chatMessage->validate());

        //message max length
        $chatMessage->message = str_repeat('a', 100);
        $this->assertTrue($chatMessage->validate());
        $chatMessage->message .= 'a';
        $this->assertFalse($chatMessage->validate());
    }

    public function testBehaviors()
    {
        Yii::app()->user->setId(1);

        $chatMessage = new ChatMessage();
        $chatMessage->message = 'message';
        $chatMessage->save();

        //author behavior
        $this->assertEquals($chatMessage->author_id, Yii::app()->user->getId());

        //timestamp behavior
        $this->assertNotEmpty($chatMessage->create_time);
    }
}
